﻿namespace MetroUILogo
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Security.Principal;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Xml.Linq;

    using Windows.ApplicationModel;
    using Windows.Management.Deployment;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            Loaded += MainWindow_Loaded;
            _list.SelectionChanged += _list_SelectionChanged;
        }

        private Tuple<string, string, string> ExtractIconFromMetroAppPackage(Package package)
        {
            var manifestPath =
                Directory.GetFiles(package.InstalledLocation.Path).FirstOrDefault(it => it.EndsWith("AppxManifest.xml"));

            if (string.IsNullOrEmpty(manifestPath))
            {
                return null;
            }

            var doc = XDocument.Load(manifestPath);

            var el2 = doc.Root.Elements().FirstOrDefault(it => it.Name.LocalName == "Applications");
            if (el2 != null)
            {
                var appEl = el2.Elements().FirstOrDefault(it => it.Name.LocalName == "Application");
                var el3 = appEl.Elements().FirstOrDefault(it => it.Name.LocalName == "VisualElements");

                var logo = el3.Attribute("Logo").Value;
                var smallLogo = el3.Attribute("SmallLogo").Value;
                var backgroundColor = el3.Attribute("BackgroundColor").Value;

                var dir = new DirectoryInfo(package.InstalledLocation.Path);
                var allFiles = dir.GetFiles("*.png", SearchOption.AllDirectories);

                //                var logoPath = allFiles.First(it => it.FullName.EndsWith(logo));
                //                var smalllogoPath = allFiles.First(it => it.FullName.EndsWith(smallLogo));
                var path = allFiles[125];
                if (path.FullName.EndsWith(smallLogo))
                {
                }

                return new Tuple<string, string, string>(logo, smallLogo, backgroundColor);
                //return new Tuple<string, string, string>(logoPath.FullName, smalllogoPath.FullName, backgroundColor);
            }

            return null;
        }

        private IEnumerable<Package> GetPackages()
        {
            var packages = new List<Package>();
            if (WindowsIdentity.GetCurrent() == null)
            {
                return packages;
            }

            var packageManager = new PackageManager();
            var metroApps = packageManager.FindPackagesForUser(WindowsIdentity.GetCurrent().User.Value);

            packages.AddRange(
                from metroApp in metroApps
                let files = Directory.GetFiles(metroApp.InstalledLocation.Path)
                let manifest = files.FirstOrDefault(it => it.EndsWith("AppxManifest.xml"))
                where !string.IsNullOrEmpty(manifest)
                select metroApp);

            return packages;
        }

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            var data = GetPackages();
            _list.ItemsSource = data;
            _list.SelectedIndex = 0;
        }

        private void _list_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count != 1)
            {
                return;
            }

            var package = e.AddedItems[0] as Package;

            var manifestInfo = WinRTManifestLogoExtracter.Extract(package);

            if (manifestInfo == null)
            {
                _backgroundColorLogo.Fill = new SolidColorBrush(Colors.Transparent);
                _backgroundColorSmallLogo.Fill = new SolidColorBrush(Colors.Transparent);
                return;
            }

            _backgroundColorLogo.Fill = (new BrushConverter().ConvertFrom(manifestInfo.BackgroundColor)) as SolidColorBrush;
            _backgroundColorSmallLogo.Fill = (new BrushConverter().ConvertFrom(manifestInfo.BackgroundColor)) as SolidColorBrush;

            var ms = new MemoryStream();
            var stream = new FileStream(manifestInfo.LogoPath, FileMode.Open, FileAccess.Read);
            ms.SetLength(stream.Length);
            stream.Read(ms.GetBuffer(), 0, (int)stream.Length);

            ms.Flush();
            stream.Close();

            var src = new BitmapImage();
            src.BeginInit();
            src.UriSource = new Uri(manifestInfo.LogoPath);
            src.EndInit();
            _imageLogo.Source = src;
        }
    }
}