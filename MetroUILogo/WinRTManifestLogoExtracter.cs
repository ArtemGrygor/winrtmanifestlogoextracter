﻿namespace MetroUILogo
{
    using System.IO;
    using System.Linq;
    using System.Xml.Linq;

    using Windows.ApplicationModel;

    public class MainfestInfo
    {
        public MainfestInfo(string logoPath, string smallLogoPath, string backgroundColor)
        {
            LogoPath = logoPath;
            SmallLogoPath = smallLogoPath;
            BackgroundColor = backgroundColor;
        }

        public string BackgroundColor { get; private set; }

        public string LogoPath { get; private set; }

        public string SmallLogoPath { get; private set; }
    }

    public class WinRTManifestLogoExtracter
    {
        public static MainfestInfo Extract(Package package)
        {
            if (package == null || package.InstalledLocation == null
                || string.IsNullOrEmpty(package.InstalledLocation.Path))
            {
                return null;
            }

            var manifestPath =
                Directory.GetFiles(package.InstalledLocation.Path).FirstOrDefault(it => it.EndsWith("AppxManifest.xml"));

            if (string.IsNullOrEmpty(manifestPath))
            {
                return null;
            }

            var xDoc = XDocument.Load(manifestPath);
            if (xDoc.Root == null)
            {
                return null;
            }

            var applicationsElement = xDoc.Root.Elements().FirstOrDefault(it => it.Name.LocalName == "Applications");
            if (applicationsElement == null)
            {
                return null;
            }

            var applicationElement =
                applicationsElement.Elements().FirstOrDefault(it => it.Name.LocalName == "Application");
            if (applicationElement == null)
            {
                return null;
            }

            var visualElement = applicationElement.Elements()
                                                  .FirstOrDefault(it => it.Name.LocalName == "VisualElements");
            if (visualElement == null)
            {
                return null;
            }

            var logo = visualElement.Attribute("Logo").Value;
            var smallLogo = visualElement.Attribute("SmallLogo").Value;
            var backgroundColor = visualElement.Attribute("BackgroundColor").Value;

            var logoPath = GetLogoPath(package.InstalledLocation.Path, logo);
            var smallLogoPath = GetLogoPath(package.InstalledLocation.Path, smallLogo);

            return new MainfestInfo(logoPath, smallLogoPath, backgroundColor);
        }

        private static string GetLogoPath(string directory, string logo)
        {
            if (string.IsNullOrEmpty(logo))
                return string.Empty;
            
            // original file
            var logoPath = Path.Combine(directory, logo);
            if (File.Exists(logoPath))
                return logoPath;

            // scale_100
            var fileName = Path.GetFileName(logo);
            var extension = Path.GetExtension(logo);
            if (string.IsNullOrEmpty(fileName) || string.IsNullOrEmpty(extension))
                return string.Empty;

            var scaleFileName = string.Format("{0}.scale-100{1}", fileName.Replace(extension, string.Empty), extension);
            logoPath = Path.Combine(directory, logo.Replace(fileName, scaleFileName));
            if (File.Exists(logoPath))
                return logoPath;

            return logo;
        }
    }
}